## Set Up

Thank you for taking the time to look at my first project
This is a to do list app made with Django, HTML, and CSS.
This project has been dockerized to help you interact with the application.
To run the project follow the commands below:

1. Make sure you're in the root of the project in your terminal
2. Run the following command:
```
docker build -t project-alpha-apr .
```
3. After the command has completed, in the terminal and run:
```
python manage.py migrate
```
4. After migrate has completed, run this command to start your containers:
```
docker run -p 8000:8000 project-alpha-apr
```

You should be able to access the project in your browser at:
```
localhost:8000
```


## Sign Up
1. Enter a username (You will be using this username when creating projects and tasks)
2. Ensure password is entered correctly both times



## My Company Page
1. Navigate to the My Compnay page from the top navigation bar
2. Create a company by enter a company name
3. After you have created a project for the company, you can view it on the company page by clicking on the created company name

## My Projects Page
1. Naviage to the My Projects page from the navigation bar
2. This page allows you to see the list of all projects
3. Create a new project by entering in details of the new project
4. For "owner" field choose your username
5. For the "company" field, please choose the company you would like to create a project for

5. After you have created a project you can click on name of project which will take you to a task page where you can create tasks for that project
6. For "assignee" field choose your username
7. You can also click on the name of created task to update it if needed.

## My Tasks Page

1. This page allows you to see all the tasks you have across multiple projects
2. On this page you can create tasks as well
3. For date fields that need to be entered, please provide dates in the following format (MM/DD/YYYY).
Example:
```
01/16/1999
```
3. For "assignee" field choose your username
5. For the "project" field, please choose the project you would like to create a task for
4. You can also click on the name of created task to update it if needed.
