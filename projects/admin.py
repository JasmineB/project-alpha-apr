from django.contrib import admin
from projects.models import Project, Company


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
        "id",
        "company",
    )

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ("name", "id", )
