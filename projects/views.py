from django.shortcuts import render, redirect
from projects.models import Project, Company
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm, CompanyForm



@login_required
def list_projects(request):
    lists = Project.objects.filter(owner=request.user)
    context = {
        "lists": lists,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    my_project = Project.objects.get(id=id)
    context = {
        "my_project": my_project,
    }

    return render(request, "projects/detail.html", context)



@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST, user=request.user)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm(user=request.user)

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)





@login_required
def company_list(request):
    companies = Company.objects.filter(company_owner=request.user)
    context = {
        "companies": companies,
    }
    return render(request, "projects/company_list.html", context)


@login_required
def company_projects(request, id):
    my_company = Company.objects.get(id=id)

    context = {
        "my_company": my_company,
    }
    return render(request, "projects/companyprojlist.html", context)

@login_required
def create_company(request):
    if request.method == "POST":
        form = CompanyForm(request.POST, user=request.user)
        if form.is_valid():
            company = form.save(commit=False)
            company.company_owner = request.user
            company.save()
            return redirect("search_company")
    else:
        form = CompanyForm(user=request.user)

    context = {
        "form": form,
    }
    return render(request, "projects/companycreate.html", context)
