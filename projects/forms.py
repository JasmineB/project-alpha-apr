from django.forms import ModelForm
from projects.models import Project, Company
from django.contrib.auth.models import User



class ProjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields['company'].queryset = Company.objects.filter(company_owner=user)
        self.fields['owner'].queryset = User.objects.filter(id=user.id)

    class Meta:
        model = Project
        fields = ['name', 'description', 'owner', 'company']



class CompanyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(CompanyForm, self).__init__(*args, **kwargs)
        self.fields['company_owner'].queryset = User.objects.filter(id=user.id)

    class Meta:
        model = Company
        fields = ['name', 'company_owner']
