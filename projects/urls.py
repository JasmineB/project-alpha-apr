from projects.views import list_projects, show_project, create_project, company_list, company_projects, create_company
from django.urls import path


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("search/", company_list, name="search_company"),
    path("company/<int:id>/", company_projects, name="company_projects"),
    path("company/", create_company, name="create_company"),

]
