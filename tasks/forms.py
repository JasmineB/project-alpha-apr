from django.forms import ModelForm
from tasks.models import Task, Project
from django.contrib.auth.models import User



class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "notes",
        ]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(TaskForm, self).__init__(*args, **kwargs)
        self.fields['project'].queryset = Project.objects.filter(owner=user)
        self.fields['assignee'].queryset = User.objects.filter(pk=user.pk)
