from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm





@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm(user=request.user)

    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)



@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks": my_tasks,
    }
    return render(request, "tasks/list.html", context)



def task_update(request, id):
    task = Task.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task, user=request.user)
        if form.is_valid():
            task = form.save()

            return redirect("show_my_tasks")
    else:
        form = TaskForm(instance=task, user=request.user)

    context = {

        "form": form,
        }
    return render(request, "tasks/update.html", context)
