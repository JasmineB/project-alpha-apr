from tasks.views import create_task, show_my_tasks, task_update
from django.urls import path

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("tasks/<int:id>/update/", task_update, name="task_update")
]
